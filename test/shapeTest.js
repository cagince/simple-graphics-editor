
import shapeFactory from '../src/components/Shape';

var assert = require('assert');


describe('shapeFactory', () => {
  describe('circle', () => {
    it('should return a circle when asked for a circle in shape factory', () => {
      let circle = shapeFactory.getCircle();
      assert.equal(circle.getType(), 'circle');
    });
  });
  describe('rectangle', () => {
    it('should return a rectangle when asked for a rectangle in shape factory', () => {
      let rect = shapeFactory.getRect();
      assert.equal(rect.getType(), 'rect');
    });
  });
  describe('nGon', () => {
    it('should return a 6-gon when asked for a 6-gon in shape factory', () => {
      let ngon = shapeFactory.getNgon(6);
      assert.equal(ngon.getType(), 'ngon');
      assert.equal(ngon.n, 6);
    });
    describe('nGon', () => {
      it('should return a 7-gon when asked for a 7-gon in shape factory', () => {
        let ngon = shapeFactory.getNgon(7);
        assert.equal(ngon.getType(), 'ngon');
        assert.equal(ngon.n, 7);
      });
    });
    describe('nGon', () => {
      it('should return a 7-gon when asked for a 7-gon in shape factory', () => {
        let ngon = shapeFactory.getNgon(7);
        assert.equal(ngon.getType(), 'ngon');
        assert.equal(ngon.n, 7);
      });
    });
    describe('nGon', () => {
      it('should return a 8-gon when asked for a 8-gon in shape factory', () => {
        let ngon = shapeFactory.getNgon(8);
        assert.equal(ngon.getType(), 'ngon');
        assert.equal(ngon.n, 8);
      });
    });
    describe('nGon', () => {
      it('should return a 9-gon when asked for a 9-gon in shape factory', () => {
        let ngon = shapeFactory.getNgon(9);
        assert.equal(ngon.getType(), 'ngon');
        assert.equal(ngon.n, 9);
      });
    });
    describe('nGon', () => {
      it('should return a 10-gon when asked for a 10-gon in shape factory', () => {
        let ngon = shapeFactory.getNgon(10);
        assert.equal(ngon.getType(), 'ngon');
        assert.equal(ngon.n, 10);
      });
    });
  });
  describe('ellipse', () => {
    it('should return a ellipse when asked for a ellipse in shape factory', () => {
      let ellipse = shapeFactory.getEllipse();
      assert.equal(ellipse.getType(), 'ellipse');
    });
  });
  describe('triangle', () => {
    it('should return a triangle when asked for a triangle in shape factory', () => {
      let triangle = shapeFactory.getTriangle();
      assert.equal(triangle.getType(), 'triangle');
    });
  });
  describe('Text', () => {
    it('should return a text when asked for a text in shape factory', () => {
      let  text = shapeFactory.getText();
      assert.equal(text.getType(), 'txt');
    });
  });
  describe('Line', () => {
    it('should return a line when asked for a line in shape factory', () => {
      let  line = shapeFactory.getLine();
      assert.equal(line.getType(), 'line');
    });
  });
  describe('Star', () => {
    it('should return a star when asked for a star in shape factory', () => {
      let  star = shapeFactory.getStar();
      assert.equal(star.getType(), 'star');
    });
  });
});



describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1,2,3].indexOf(4), -1);
    });
  });
});
