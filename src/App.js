import React, { Component } from 'react';
import Canvas from "./components/Canvas";

//styles
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Canvas />
      </div>
    );
  }
}

export default App;
