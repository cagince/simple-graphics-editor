import React, {Component} from 'react';
import Sidebar from './Sidebar';
import ColorPicker from './ColorPicker';

import ShapeFactory from './Shape';
import Layer from './Layer';
import $ from 'jquery';

class Canvas extends Component {

  constructor() {
    super();


    // BIND FUNCTIONS TO CANVAS
    // #######################################
    // add shapes to canvas
    this.addShape = this.addShape.bind(this)
    // shape-change handlers
    this.handleNgonSubmit = this.handleNgonSubmit.bind(this);
    this.handleTextSubmit = this.handleTextSubmit.bind(this);
    this.handleTextChange = this.handleTextSubmit.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.editText = this.editText.bind(this);
    // mouse triggers
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);
    // canvas manipulation
    this.drawCanvas = this.drawCanvas.bind(this);
    this.clearCanvas = this.clearCanvas.bind(this);
    this.clearAll = this.clearAll.bind(this);
    this.deleteSelected = this.deleteSelected.bind(this);

    // export funct.
    this.downloadImg = this.downloadImg.bind(this);
    // Layer manipulation
    this.addLayer = this.addLayer.bind(this);
    this.toggleLayerVisibility = this.toggleLayerVisibility.bind(this);
    this.createNewLayer = this.createNewLayer.bind(this);
    this.onLayerClick = this.onLayerClick.bind(this);

    // default state
    this.state = {
      customizeNgon: false,
      customizeText: false,
      text: {
        t: '',
        s: 50,
      },
      interval: 100,
      canvas: null,
      ctx: null,
      valid: false,
      shapes: [],
      dragging: false,
      selection: null,
      color: 'rgba(0,255,127,0.2)',
      dragoffx: 0,
      dragoffy: 0,
      mx: 0,
      my: 0,
      layers: {
        array: [],
        currentLayer: null,
      },
    };
  }

  // MOUSE TRIGGERS
  onMouseUp(e) {
    e.preventDefault();
    this.setState({
      dragging: false,
      scaling: false,
      scaleDirections: null,
    });
  }
  onMouseDown(e) {
    // mouse x y sini al
    const rect = this.state.canvas.getBoundingClientRect();
    const mx = e.clientX - rect.left;
    const my = e.clientY - rect.top;
    // check if shape was selected
    if (this.state.selection) {
      const scaleDirections = this.state.selection.checkScaling(mx, my);
      // check if selected item is being scaled
      if (scaleDirections.includes(true)) {
        this.setState({
          dragoffx: this.state.mx - this.state.selection.x,
          dragoffy: this.state.my - this.state.selection.y,
          dragging: false,
          scaling: true,
          scaleDirections,
          valid: false,
        });
        return;
      } else {
         this.setState({
           selection: null,
           valid: false,
         });
      }
    }

    this.state.layers.currentLayer.shapes.forEach((s) => {
      if (s.type === 'line' ?
        s.contains(this.state.ctx, mx, my)
        : s.contains(mx, my)) {
        this.setState({
          dragoffx : this.state.mx - s.x,
          dragoffy : this.state.my - s.y,
          dragging : true,
          selection : s,
          // color: s.fill,
          valid : false,
        });
        return;
      }
    });
  }
  onMouseMove(e) {
    // mouse x ve y sini al
    const rect = this.state.canvas.getBoundingClientRect();
    const mx = e.clientX - rect.left;
    const my = e.clientY - rect.top;
    this.setState({ mx, my }); // canvas a bildir
    if (this.state.dragging) {
      if (this.state.selection) {
        this.state.selection.drag(mx, my, this.state.dragoffx, this.state.dragoffy);
        this.setState({
          valid: false,
        });
      }
    } else if (this.state.scaling) {
      if (this.state.selection.type === 'txt') {
        this.editText(this.state.selection);
      }else {
        this.state.selection.scale(mx, my, this.state.scaleDirections);

      }
      this.setState({
        valid: false
      });
    }
  }


  // LAYER MANIPULATIONS
  onLayerClick(id) {
    this.setState((prevState) => {
      return {
        layers: {
          array: prevState.layers.array,
          currentLayer: prevState.layers.array[id],
          max: 3,
        },
      };
    });
  }
  createNewLayer() {
    // QR6 : Proxy pattern
    const newLayer = new Proxy(new CanvasLayer(this.state.layers.array.length), {
      get: (target, name, receiver) =>  {
        if (name === 'addShape') {
          console.log(`new shape added to layer ${target.id}`);
        }
        if (name === 'deleteShape') {
          console.log(`a shape deleted from layer ${target.id}`);
        }
        return Reflect.get(target, name, receiver);
      },
    });

    this.state.layers.currentLayer = newLayer;
    this.state.layers.array.push(newLayer);
  }
  toggleLayerVisibility(e) {
    e.preventDefault();
    this.state.layers.array[e.target.value].toggleVisibility();
  }


  // initialize
  init() {
    const canvas = document.getElementById('canvas');
    if (canvas.getContext) {
      const ctx = canvas.getContext('2d');
      canvas.width = window.innerWidth;
      canvas.height = window.innerHeight;
      this.setState({
        canvas,
        ctx,
      });
      // draw canvas in each interval ms.
      window.setInterval(() => { this.drawCanvas(); }, this.state.interval);
    }
    this.createNewLayer();
  }


  /**
   *  Add shapes
   *  #QR6 : Abstract Method Pattern
   * */
  addShape(type) {
    let s = null;
    switch (type) {
      case 'text':
        s = ShapeFactory.getText();
        this.editText(s);
        break;
      case 'rect':
        s = ShapeFactory.getRect();
        break;
      case 'star':
        s = ShapeFactory.getStar();
        break;
      case 'ngon':
        this.setState({ customizeNgon: true });
        break;
      case 'circle':
        s = ShapeFactory.getCircle();
        break;
      case 'ellipse':
        s = ShapeFactory.getEllipse();
        break;
      case 'line':
        s = ShapeFactory.getLine();
        break;
      case 'triangle':
        s = ShapeFactory.getTriangle();
        break;
      default:

    }
    if (s) {
      s.setColor(this.state.color);
      s.draw(this.state.ctx);
      this.state.layers.currentLayer.addShape(s);
      this.state.selection = s;
    }
  }
  // Shape Edit handlers
  handleNgonSubmit(e) {
    e.preventDefault();
    let s = ShapeFactory.getNgon(this.refs.edgeSelector.value);
    s.setColor(this.state.color);
    s.draw(this.state.ctx);
    this.state.layers.currentLayer.addShape(s);
    this.setState({ customizeNgon : false })
  }
  handleTextSubmit(e) {
    e.preventDefault();
    this.setState({ customizeText : false });
    this.state.selection.setTextAndSize(this.refs.textEdit.value, this.refs.fontSize.value);
  }
  handleTextChange(e) {
    e.preventDefault();
  }
  editText(t) {
    this.setState({
      customizeText: true,
      text: {
        t: t.text,
        s: t.size,
      },
    });
  }


  // Download as SVG
  downloadImg() {
    let children = '';
    this.state.layers.array.forEach(l => {
      l.shapes.forEach(s => {
        console.log(s.toString());
        children += s.toString();
      });
    })
    let svg = `
      <svg width="${window.innerWidth}" height="${window.innerHeight}">
        ${children}
      </svg>
    `;
    let xml = `<?xml version="1.0" encoding="utf-8" standalone="no"?>
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
    <svg xmlns="http://www.w3.org/2000/svg" width="${window.innerWidth}" height="${window.innerHeight}" xmlns:xlink="http://www.w3.org/1999/xlink">
      <source>
        <![CDATA[hello world]]>
      </source>
      ${children}
    </svg>`;
    this.state.dw= true;

    let a = $('#download');
    a.text("download");
    a.attr("download", "diagram.svg"); // TODO I could put title here
    a.attr("href", "data:image/svg+xml," + encodeURIComponent(xml));
  }

  drawCanvas() {
    if (!this.state.valid) {
      this.clearCanvas();
      // draw all shapes
      let layers = new Iterator(this.state.layers.array);
      layers.each((l) => {
        if (l.isVisible()) {
          let iterator = new Iterator(l.shapes);
          iterator.each((s) => {
            let selected = false;
            if (
              !(s.x > this.state.canvas.width
              || s.y > this.state.canvas.height
              || s.x + s.w < 0 || s.y + s.h < 0)){
              if (this.state.selection
                && s === this.state.selection)
                selected = true;
              s.draw(this.state.ctx,selected);
            }
          })
        }
      })
      this.valid = true;
    }
  }
  clearAll(){
    this.state.layers.currentLayer.shapes=[];
  }

  clearCanvas() {
    this.state.ctx.clearRect(0,0,this.state.canvas.width, this.state.canvas.height);
  }

  // remove selected item from canvas
  deleteSelected() {
    if (this.state.selection) {
      this.state.layers.currentLayer.deleteShape(this.state.selection);
    }
  }

  handleColorChange(color) {
    this.setState({
      color: `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`
    });
    if (this.state.selection) this.state.selection.setColor(this.state.color);
  }

  addLayer() {
    if (this.state.layers.array.length <= 3) {
      this.createNewLayer();
    } else {
      console.error("max layers reached");
    }
  }

  componentDidMount() {
    this.init();
  }
  render() {
    return (
      <div>
        <div id="canvasDiv" className="canvas_wrapper">
          <canvas
            id="canvas"
            width="1600"
            height="500"
            onMouseDown={this.onMouseDown}
            onMouseMove={this.onMouseMove}
            onMouseUp={this.onMouseUp}
          ></canvas>
        </div>

        { this.state.customizeNgon &&
          <div className="customizeForm">
            <form onSubmit={this.handleNgonSubmit}>
                <select ref="edgeSelector" name="Edges">
                 <option value="5">5</option>
                 <option value="6">6</option>
                 <option value="7">7</option>
                 <option value="8">8</option>
                 <option value="9">9</option>
                 <option value="10">10</option>
               </select>
               <button onClick={this.handleNgonSubmit}> add </button>
            </form>
           </div>

        }
        { this.state.customizeText &&
          <div className="customizeForm">
            <form onSubmit={this.handleTextSubmit}>
                <p> Text: </p>
                <input ref="textEdit" defaultValue={this.state.text.t} />
                <p> font size : </p>
                <input ref="fontSize" defaultValue={this.state.text.s} />
                <br />
               <button onClick={this.handleTextSubmit}>submit</button>
            </form>
           </div>

        }

        <Sidebar
          addShape={this.addShape}
          color={this.state.color}
          >
        <div className="colorPicker">
          <p>
            Color :
          </p>
          <ColorPicker
            color={{r:0, g:255, b:127, a:0.2}}
            extractColor={this.handleColorChange}
            />
            <div className="clear"></div>
        </div>
            {this.state.dragging &&
              <p>dragging</p>
            }
            {this.state.valid &&
              <p>valid</p>
            }
          <Layer
            layers={this.state.layers}
            createNewLayer={this.createNewLayer}
            toggleLayerVisibility={this.toggleLayerVisibility}
            addLayer={this.addLayer}
            onLayerClick={this.onLayerClick}
          />
          <div className="action-buttons-wrapper">
          <hr/>
          <button className="fw-button" onClick={this.deleteSelected}> delete </button>
          <br/>
          <button className="fw-button" onClick={this.clearAll}> clear </button>
          <hr/>
          <button className="fw-button" onClick={this.downloadImg}>Export as SVG</button>
          <a id="download" />
          <hr/>
          </div>
          <h6 className="mouse">
            Mouse: {this.state.mx} :
            {this.state.my}
          </h6>
        </Sidebar>
      </div>
    );
  }
}

class Iterator {
  constructor(items) {
    this.index = 0;
    this.items = items;
  }
  next() { return this.items[this.index++]; }
  hasNext() { return this.index <= this.items.length; }
  reset() { this.index = 0; }
  first() {
    this.reset();
    return this.next();
  }
  each(callback) {
    for (let item = this.first(); this.hasNext(); item = this.next()) {
      callback(item);
    }
  }
}

class CanvasLayer {
  constructor(id) {
    this.shapes = [];
    this.visible = true;
    this.id = id;
  }
  toggleVisibility() {
    this.visible = !this.visible;
  }
  isVisible() {
    return this.visible;
  }
  addShape(shape) {
    this.shapes.push(shape);
  }
  deleteShape(shape) {
    this.shapes.splice(this.shapes.indexOf(shape), 1);
  }
}


export default Canvas;
