import React, { Component } from 'react';

class Layer extends Component {
  constructor() {
    super();
    this.LayerList = this.LayerList.bind(this);
  }

  handleLayerClick(id) {
    this.props.onLayerClick(id);
  }
  LayerList() {
    if (this.props.layers && this.props.layers.array.length <= 0) {
      return null;
    }
    const listItems = this.props.layers.array.map((layer, index) =>
      <li
        onClick={this.handleLayerClick.bind(this, index)}
        className={`layer-item ${index === this.props.layers.currentLayer.id}`} key={index}>
        <input onChange={this.props.toggleLayerVisibility} type="checkbox" id="layer" name="layer" value={index} checked={layer.visible} />
        <label>layer-{index}</label>
      </li>
    );
    return (
      <ul>
        <li className="layer-item">
          <a href="javascript:;" onClick={this.props.addLayer}> Add Layer </a>
        </li>
        {listItems}
      </ul>
    );
  }

  render() {
    const layerList = this.LayerList()


    return (
      <div className="layers">
        <h6> Layers </h6>
        {layerList ? layerList : 'initializing...'}
      </div>
    );
  }
}
export default Layer;
