import drawingStrategies from './Strategies'; // strategy Pattern
import {  makeDraggable } from './draggability'; // decorator Pattern

const types = ['rect', 'circle', 'ellipse', 'triangle', 'ngon', 'text', 'line', 'star'];
const switchCase = cases => defaultCase => key => cases.hasOwnProperty(key) ? cases[key] : defaultCase;

// Another Decorator Pattern => ensures that data is not changed afterwards
function readonly(target, key, descriptor) {
  descriptor.writable = false;
  descriptor.configurable = false;
  return descriptor;
}
// Abstract Factory Pattern START ----> 
/**
 * returns : 
 *   icerisinde sadece fonksiyonlar olan bir obje.
 */
const concreteShapeFactory = (callback) => ({
  getNgon     : (edges) => getShape('ngon', edges, 100, 100),
  getRect     : () => getShape('rect',100, 100, 100, 100),
  // getRect : function() {
  //   return getShape('rect',100, 100, 100, 100);
  // }
  getCircle   : () => getShape('circle', 100, 100, 50),
  getEllipse  : () => getShape('ellipse', 100, 100, 50, 100),
  getTriangle : () => getShape('triangle', 200, 200),
  getText     : () => getShape('txt', ' ', 50, 200, 200),
  getLine     : () => getShape('line', 100, 100, 250, 250),
  getStar     : () => getShape('star', 200, 200, 50),
});
const ShapeFactory = concreteShapeFactory((type) =>  console.log(`a ${type} added using Factory Pattern` ));
/**
 *  ShapeFactory.getRect();
    -> concreteShapeFactory.getRect(); 
    -----> getShape('rect',100, 100, 100, 100),
    --------> type = 'rect' 
              -->  return new Rectangle(100, 100, 100, 100s);
                    dipnot ...args = 100, 100, 100, 100s
 */


// factory pattern => decide on which shape to return depending on the given argument
// switch (type) {
//   case 'rect':
//     return new Rectangle(...args);
// }
// Factory icin yardimci method
const getShape = (type, ...args) => ({
  'rect': new Rectangle(...args),
  'circle': new Circle(...args),
  'ellipse': new Ellipse(...args),
  'triangle': new Triangle(...args),
  'ngon': new NGon(...args),
  'txt': new Txt(...args),
  'line': new Line(...args),
  'star': new Star(...args),
})[type];

// <---- Abstract Factory Pattern END

// Abstract Shape Class...
class Shape {
  constructor() {
    // new Shape(); tanimlanmasini engelliyoruz
    if (new.target === Shape)  throw new TypeError("Cannot construct Abstract instances directly");
    this.fill = 'rgba(0,255,127,0.2)';
    this.stroke = 'black';
  }

  // default methods
  getType() { return this.type; }
  draw = (ctx, selected) => drawingStrategies[this.type].draw(this, ctx, selected); // STRATEGY PATTERN
  /**
   * draw
   * 
   */
  updateProps = (options) => Object.keys(options).forEach(key => this[key] = options[key]);
  /**
   * options = { x: 100, y: 100, width: 100, height: 100}
   * this.x = x;
   * this.y = y;
   * this.width = width;
   * this.height = height;
   * 
   * updateProps({ x, y, width, height});
   */
  @readonly checkCloseEnough(p1, p2){ return Math.abs(p1 - p2) < 15; }
  @readonly setColor(color) { this.fill = color; }
  // let triangle = new Triangle();
  // triangle.drag = () => { console.log('cannot drag')}; // success 
  // triangle.setColor = () => {  console.log('cannot set color')} // error 
  // triangle.checkCloseEnough = () => {  console.log('cannot set checkclose Enough')} // Error





}




// Triangle shape
class Triangle extends Shape {
  constructor(x, y) {
    super();
    this.updateProps({ type: 'triangle', x, y,  p1 : [x, y + 50], p2 : [x - 50, y - 50], p3 : [x + 50, y - 50] });
    makeDraggable(this);
  }

  toString() {
    return `<polygon points="${this.p1.toString},${this.p2.toString},${this.p3.toString}" style="fill:${this.fill};" />`;
  }
  area(x1, y1, x2, y2, x3, y3) {
    return Math.abs((
      x1 * (y2 - y3) +
      x2 * (y3 - y1) +
      x3  * (y1 - y2)) /2.0
    );
  }
  contains(mx, my) {
    const a = this.area(this.p1[0], this.p1[1], this.p2[0], this.p2[1], this.p3[0], this.p3[1]);
    const a1 = this.area(mx, my, this.p2[0], this.p2[1], this.p3[0], this.p3[1]);
    const a2 = this.area(this.p1[0], this.p1[1], mx, my, this.p3[0], this.p3[1]);
    const a3 = this.area(this.p1[0], this.p1[1], this.p2[0], this.p2[1], mx, my);
    return (a === a1 + a2 + a3);
  }
  initCenter() {
    this.x = (this.p1[0] + this.p2[0] + this.p3[0]) / 3;
    this.y = (this.p1[1] + this.p2[1] + this.p3[1]) / 3;
  }
  checkScaling(mx, my) {
    const scaling = [
      false, // left
      false, // top
      false, // back
    ];
    if (
      this.checkCloseEnough(mx, this.p1[0])
      && this.checkCloseEnough(my, this.p1[1])) {
      scaling[0] = true;
    } else if (
      this.checkCloseEnough(mx, this.p2[0])
      && this.checkCloseEnough(my, this.p2[1])) {
      scaling[1] = true;
    } else if (
      this.checkCloseEnough(mx, this.p3[0])
      && this.checkCloseEnough(my, this.p3[1])) {
      scaling[2] = true;
    }
    return scaling;
  }
  scale(mx, my, scalers) {
    if (scalers[0]) {
      // drawScaler(ctx, obj.x - obj.radius , obj.y); // left
      this.p1 = [mx, my];
    } else if (scalers[1]) {
      this.p2 = [mx, my];
    } else if (scalers[2]) {
      this.p3 = [mx, my];
    }
    this.initCenter();
  }
}
class Circle extends Shape {
  constructor(x, y, radius) {
    super();
    this.updateProps({ type: 'circle', x, y, radius });
    makeDraggable(this);
  }
  contains = (mx, my)  =>  Math.abs(this.x - mx) < this.radius && Math.abs(this.y - my ) < this.radius; 
  checkScaling(mx, my) {
    const scaling = new Array(4).fill(false);
    if (
      this.checkCloseEnough(mx, this.x - this.radius)
      && this.checkCloseEnough(my, this.y)) {
      scaling[0] = true;
    } else if (
      this.checkCloseEnough(mx, this.x)
      && this.checkCloseEnough(my, this.y - this.radius)) {
      scaling[1] = true;
    } else if (
      this.checkCloseEnough(mx, this.x + this.radius)
      && this.checkCloseEnough(my, this.y)) {
      scaling[2] = true;
    } else if (
      this.checkCloseEnough(mx, this.x)
      && this.checkCloseEnough(my, this.y + this.radius)) {
      scaling[3] = true;
    }
    return scaling;
  }
  scale(mx, my, scalers) {
    if (scalers[0]) {
      // drawScaler(ctx, obj.x - obj.radius , obj.y); // left
      if (mx > this.x - this.radius) {
        this.radius = this.radius >= 30 ? this.radius - 2 : this.radius;
      } else {
        this.radius = this.radius < 1000 ? this.radius + 2 : this.radius;
      }
    } else if (scalers[1]) {
      // drawScaler(ctx, obj.x, obj.y - obj.radius); // top
      if (my > this.y - this.radius) {
        this.radius = this.radius >= 30 ? this.radius - 2 : this.radius;
      } else {
        this.radius = this.radius < 1000 ? this.radius + 2 : this.radius;
      }
    } else if (scalers[2]) {
      // drawScaler(ctx, obj.x + obj.radius, obj.y); // right
      if (mx > this.x + this.radius) {
        this.radius = this.radius < 1000 ? this.radius + 2 : this.radius;
      } else {
        this.radius = this.radius >= 30 ? this.radius - 2 : this.radius;
      }
    } else if (scalers[3]) {
      // drawScaler(ctx, obj.x, obj.y + obj.radius); // bottom
      if (my > this.y + this.radius) {
        this.radius = this.radius < 1000 ? this.radius + 2 : this.radius;
      } else {
        this.radius = this.radius >= 30 ? this.radius - 2 : this.radius;
      }
    }
  }
  toString = () => `<circle cx="${this.x}" cy="${this.y}" r="${this.radius}" style="fill:${this.fill};" />` ; 
}

class Rectangle extends Shape {
  constructor(x, y, width, height) {
    // 100, 100, 100, 100
    super();
    this.updateProps({ type: 'rect', x, y, width, height });
    makeDraggable(this);
  }
  contains(mx, my) {
    return (this.x <= mx) && (this.x + this.width >= mx)
    && (this.y <= my) && (this.y + this.height >= my);
  }
  checkScaling(mx, my) {
    const scaling = [
      false, // tl
      false, // tr
      false, // bl
      false, // br
    ];
    if (
      this.checkCloseEnough(mx, this.x)
      && this.checkCloseEnough(my, this.y)) {
      scaling[0] = true;
    } else if (
      this.checkCloseEnough(mx, this.x + this.width)
      && this.checkCloseEnough(my, this.y)) {
      scaling[1] = true;
    } else if (
      this.checkCloseEnough(mx, this.x)
      && this.checkCloseEnough(my, this.y + this.height)) {
      scaling[2] = true;
    } else if (
      this.checkCloseEnough(mx, this.x + this.width)
      && this.checkCloseEnough(my, this.y + this.height)) {
      scaling[3] = true;
    }
    return scaling;
  }
  scale(mx, my, scalers) {
    if (scalers[0]) {
      this.width += this.x - mx;
      this.height += this.y - my;
      this.x = mx;
      this.y = my;
    } else if (scalers[1]) {
      this.width = Math.abs(this.x - mx);
      this.height += this.y - my;
      this.y = my;
    } else if (scalers[2]) {
      this.width += this.x - mx;
      this.height = Math.abs(this.y - my);
      this.x = mx;
    } else if (scalers[3]) {
      this.width = Math.abs(this.x - mx);
      this.height = Math.abs(this.y - my);
    }
  }
  toString() {
    return `<rect x="${this.x}" y="${this.y}" width="${this.width}" height="${this.height}"
            style="fill:${this.fill};" />`
  }
}
class NGon extends Shape {
  constructor(n, x, y) {
    super();
    this.updateProps({ type : 'ngon', size: 100, n, x, y, edges:[] });
    this.initEdges();
    makeDraggable(this);

  }

  initEdges() {
    this.edges = [];
    for (let i = 1; i <= this.n; i++) {
      // Determine the coordinates of the next vertex
      this.edges.push([
        this.x + this.size * Math.cos(2 * Math.PI * i / this.n),
        this.y + this.size * Math.sin(2 * Math.PI * i / this.n)
      ]);
    }
  }



  contains(mx, my) {
    return Math.abs(this.x - mx) < this.size && Math.abs(this.y - my) < this.size;
  }

  checkScaling(mx, my) {
    const scaling = [];
    this.edges.forEach((e) => {
      scaling.push((this.checkCloseEnough(mx, e[0]) && this.checkCloseEnough(my, e[1])));
    });
    return scaling;
  }

  scale(mx, my, scalers) {
    const dx = mx - this.x;
    const dy = my - this.y;
    const dist = Math.sqrt((dx * dx) + (dy * dy));
    if (dist < this.size) {
      this.size -= 2;
    } else {
      this.size += 2;
    }
    this.initEdges();
  }
  toString() {
    return `<polygon points="${this.edges.toString()}" style="fill:${this.fill};" />`;
  }
}
class Txt extends Shape {
  constructor(text, size, x, y) {
    super();
    this.updateProps({ type : 'txt', text, size, x, y, rect : new Rectangle(x, y-size, text.length* size/2, size) });
    this.initRect();
    makeDraggable(this);
  }

  initRect() {
    const { x, y, size, text } = this;
    this.rect = new Rectangle(x, y-size, text.length* size/2, size);
  }
  setTextAndSize(text,size){
    this.updateProps({ text, size });
    this.initRect();
  }
  contains(mx, my) {
    const {x ,y, width, height } = this.rect;
    return (x <= mx) && (x + width >= mx) && (y <= my) && (y + height >= my);
  }
  checkScaling(mx, my) { return this.checkCloseEnough(mx, this.rect.x) && this.checkCloseEnough(my, this.rect.y) ? [true] :[false]; }
  toString() { return `<text x="${this.x}" y="${this.y}" fill="${this.fill}">${this.text}</text>` }

}
class Line extends Shape {
  constructor(x1, y1, x2, y2) {
    super();
    this.updateProps({x1, y1, x2, y2, type: 'line'});
    makeDraggable(this);
  }
  toString() { return `<line x1="${this.x1}" y1="${this.y1}" x2="${this.x2}" y2="${this.y2}" style="stroke:${this.fill};stroke-width:2" />` }
  contains(ctx, mx, my) {
    ctx.beginPath(); // new segment
    ctx.lineWidth = 9;
    ctx.moveTo(this.x1, this.y1); // start is current point
    ctx.lineTo(this.x2, this.y2); // start is current point
    return ctx.isPointInStroke(mx, my);
  }

  checkScaling(mx, my) {
    const scaling = new Array(2).fill(false);
    if (this.checkCloseEnough(mx, this.x1) && this.checkCloseEnough(my, this.y1)) { scaling[0] = true; } 
    else if (this.checkCloseEnough(mx, this.x2) && this.checkCloseEnough(my, this.y2)) {scaling[1] = true; }
    return scaling;
  }
  scale(mx, my, scalers) {
    if (scalers[0]) this.updateProps({ x1: mx, y1: my });
    if (scalers[1]) this.updateProps({ x2: mx, y2: my });
  }
}
class Star extends Shape {
  constructor(x, y, radius) {
    super();
    this.updateProps({ type : 'star', x, y, outerRadius : radius, innerRadius : (radius / 2), spikes : 5 });
    makeDraggable(this);
  }

  toString = () => `<polygon points="${this.getEdges()}" style="fill:${this.fill};" />`;
  contains = (mx, my) => Math.abs(this.x - mx) < this.outerRadius && Math.abs(this.y - my) < this.outerRadius;
  checkScaling = (mx, my) => [this.checkCloseEnough(mx, this.x) && this.checkCloseEnough(my, this.y - this.outerRadius)];
  getEdges() {
    let { x, y, spikes, outerRadius, innerRadius } = this;
    let edges = [];
    let rot = Math.PI / 2 * 3;
    let step = Math.PI / spikes;
    edges.push([x,y - outerRadius]);
    new Array(spikes).forEach(i => {
      x = x + Math.cos(rot) * outerRadius;
      y = y + Math.sin(rot) * outerRadius;
      edges.push([x, y]);
      rot += step;
      x = x + Math.cos(rot) * innerRadius;
      y = y + Math.sin(rot) * innerRadius;
      edges.push([x, y]);
      rot += step;
    });
    edges.push( [x, y - outerRadius]);
    return edges.toString();
  }
  scale(mx, my, scalers) {
    const { x, y, outerRadius, innerRadius } = this;
    const dx = mx - x;
    const dy = my - y;
    const dist = Math.sqrt((dx * dx) + (dy * dy));
    this.updateProps({
      outerRadius: outerRadius + (dist < outerRadius ? -2 : 2),
      innerRadius: outerRadius / 2
    });
  }
}
class Ellipse extends Shape {
  constructor(x, y, radiusY, radiusX) {
    super();
    this.updateProps({ type: 'ellipse', x, y, radiusX, radiusY });
    makeDraggable(this);
  }
  toString = () => `<ellipse cx="${this.x}" cy="${this.y}" rx="${this.radiusX}" ry="${this.radiusY}" style="fill:${this.fill};" />`
  contains = (mx, my) => Math.abs(this.x - mx) < this.radiusX && Math.abs(this.y - my) < this.radiusY;
  checkScaling(mx, my) {
    const { checkCloseEnough, x, y, radiusX, radiusY} = this;
    return [
      checkCloseEnough(mx, this.x - radiusY) && checkCloseEnough(my, y),
      checkCloseEnough(mx, this.x) && checkCloseEnough(my, y - radiusX),
      checkCloseEnough(mx, this.x + radiusY) && checkCloseEnough(my, y),
      checkCloseEnough(mx, this.x) && checkCloseEnough(my, y + radiusX)
    ];
  }
  scale(mx, my, ss) {
    let { x, y, radiusX: rx, radiusY: ry } = this;
    const sum = (n, m) => n + m;
    const dif = (n, m) => n - m;
    const scr = (r) => r < 1000 ? r + 2 : r; // scale
    const shr = (r) => r >= 30 ? r - 2 : r; // shrink
    const is_sc = (v, g) => v > g;
    const s_o_s = (sh, r,d) => (!d ? !sh: sh) ? shr(r) : scr(r);
    const f = (m, v, r, d) => s_o_s(is_sc(m, d ? dif(v,r) : sum(v,r)),r,d)
    if (ss[0]) ry = f(mx, x, ry, true);
    if (ss[1]) rx = f(my, y, rx, true);
    if (ss[2]) ry = f(mx, x, ry, false);
    if (ss[3]) rx = f(my, y, rx, false);
    this.updateProps({ radiusX: rx, radiusY: ry });
  }
}

export default ShapeFactory;
