

const switchCase = cases => defaultCase => key => cases.hasOwnProperty(key) ? cases[key] : defaultCase;


// export const makeDraggable = (shape) => shape.drag = getDragger(shape);
/**
 * Draggable olmayan bir sekli makeDraggable(sekil) diyerek drag methodunu ekliyoruz.
 * ve bu fonksiyonun icerisinde this. kullanabilmek icin bind(sekil) diyoruz
 *
 */
export const makeDraggable = (shape) => shape.drag = draggers[draggers.hasOwnProperty(shape.type) ? shape.type : 'default'].bind(shape);

// function makeDraggable(shape) {
//     // shape.type = 'circle'
//     if(draggers.hasOwnProperty(shape.type)) {
//         let drag = dragger[shape.type];
//     } else {
//         let drag = dragger['default'];
//     }
//     shape.drag = drag.bind(shape);
// }
// 
// 
// class asd {
//     this.x = 3;
//     this.y = 4;
//    
// }
// function f1() {
//     this.x = 3;
//     this.y = 4;
//     return this.x + this.y;
// }
// 
// 
// 
// let a = new asd();
// 
// asd.f1 = f1.bind(asd);
// 
// a.f1();


const draggers = {
    'default' : function(dx,dy) { this.updateProps({ x: dx, y: dy})},
    'rect': function(x, y, dx, dy) { this.updateProps({ x: x - dx, y: y - dy }) },
    'ngon': function(dx, dy) { this.updateProps({x: dx, y: dy}); this.initEdges(); },  
    'txt': function(x, y, dx, dy) { 
        this.updateProps({ x: x - dx, y: y - dy }); 
        this.initRect();
    },
    'line': function(dx, dy) {
        const { x1, x2, y1, y2 } = this;
        const diff = [ [dx - x1, dy - y1], [dx - x2, dy - y2] ]
        this.updateProps({ x1: dx + diff[0][0], x2: dx + diff[1][0], y1: dy + diff[0][1], y2: dy + diff[1][1] });
    },
    'triangle' : function (dx,dy) {
        const {x, y, p1, p2, p3} = this;
        const diff = [
            [ (x < p1[0] ? -1 : 1) * Math.abs(x - p1[0]), (y < p1[1] ? -1 : 1) * Math.abs(y - p1[1]) ],
            [ (x < p2[0] ? -1 : 1) * Math.abs(x - p2[0]), (y < p2[1] ? -1 : 1) * Math.abs(y - p2[1]) ],
            [ (x < p3[0] ? -1 : 1) * Math.abs(x - p3[0]), (y < p3[1] ? -1 : 1) * Math.abs(y - p3[1]) ],
        ];
        this.updateProps({
            p1 : [dx - diff[0][0], dy - diff[0][1]],
            p2 : [dx - diff[1][0], dy - diff[1][1]],
            p3 : [dx - diff[2][0], dy - diff[2][1]],
            x : dx,
            y : dy,
        });
    },
};
