import React, { Component } from 'react';
import logo from '../logo.svg';

class Sidebar extends Component {
  constructor(){
    super();
  }
  handleClick(e){
    console.log(e);
    // this.props.addShape(e.);
  }


  render() {
    return (
      <div className="sidebar">
        <ul>
           <li onClick={(e) => {this.props.addShape('line')}}>
             <img className="line" alt="" src="./img/vector.png" />
           </li>
           <li onClick={(e) => {this.props.addShape('circle')}}>
             <img className="circle" alt="" src="./img/oval.png" />
           </li>
           <li onClick={(e) => {this.props.addShape('ellipse')}}>
             <img className="elipse" alt="" src="./img/elipse.png" />
           </li>
           <li onClick={(e) => {this.props.addShape('triangle')}}>
             <img className="triangle" alt="" src="./img/triangle.png" />
           </li>
           <li onClick={(e) => {this.props.addShape('rect')}}>
             <img className="square" alt="" src="./img/square.png" />
           </li>
           <li onClick={(e) => {this.props.addShape('ngon')}}>
             <img className="pentagon" alt="" src="./img/polygon.png" />
           </li>
           <li onClick={(e) => {this.props.addShape('star')}}>
             <img className="star" alt="" src="./img/astronomy.png" />
           </li>
           <li onClick={(e) => {this.props.addShape('text')}}>
             <img className="text" alt="" src="./img/font.png" />
           </li>
         </ul>
         <hr/>
        { this.props.children }
      </div>
    );
}
}

export default Sidebar;
