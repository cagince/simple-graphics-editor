const drawScaler = (ctx, x, y) => {
  ctx.save();
  ctx.fillStyle = '#FF0000';
  ctx.beginPath();
  ctx.arc(x, y, 5, 0, 2 * Math.PI);
  ctx.fill();
  ctx.closePath();
  ctx.restore();
};





/**
#QR6 Strategy Pattern In Detail.
Strategy pattern for drawing objects
a specific strategy will be choosen each time when a type of a shape is defined
* */
const drawingStrategies = {
  rect: {
    draw: (obj, ctx, selected) => {
      ctx.fillStyle = obj.fill;
      ctx.fillRect(obj.x, obj.y, obj.width, obj.height);
      if (selected) {
        ctx.strokeStyle = obj.stroke;
        ctx.lineWidth = 1;
        ctx.strokeRect(obj.x, obj.y, obj.width, obj.height);
        drawScaler(ctx, obj.x, obj.y);
        drawScaler(ctx, obj.x + obj.width, obj.y);
        drawScaler(ctx, obj.x + obj.width, obj.y + obj.height);
        drawScaler(ctx, obj.x, obj.y + obj.height);
      }
    },
  },
  circle: {
    draw: (obj, ctx, selected) => {
      ctx.fillStyle = obj.fill;
      ctx.beginPath();
      ctx.arc(obj.x, obj.y, obj.radius, 0, 2 * Math.PI);
      ctx.closePath();
      ctx.fill();
      if (selected) {
        ctx.strokeStyle = obj.stroke;
        ctx.lineWidth = 1;
        ctx.stroke();
        drawScaler(ctx, obj.x - obj.radius, obj.y); // left
        drawScaler(ctx, obj.x, obj.y - obj.radius); // top
        drawScaler(ctx, obj.x + obj.radius, obj.y); // right
        drawScaler(ctx, obj.x, obj.y + obj.radius); // top
      }
    },
  },
  ellipse: {
    draw: (obj, ctx, selected) => {
      ctx.fillStyle = obj.fill;
      ctx.beginPath();
      ctx.ellipse(obj.x, obj.y, obj.radiusX, obj.radiusY, Math.PI / 2, 0, 2 * Math.PI);
      ctx.stroke();
      ctx.fill();
      ctx.closePath();
      if (selected) {
        ctx.strokeStyle = obj.stroke;
        ctx.lineWidth = 1;
        ctx.stroke();
        drawScaler(ctx, obj.x - obj.radiusY, obj.y); // left
        drawScaler(ctx, obj.x, obj.y - obj.radiusX); // top
        drawScaler(ctx, obj.x + obj.radiusY, obj.y); // right
        drawScaler(ctx, obj.x, obj.y + obj.radiusX); // bottom
      }
    },
  },
  triangle: {
    draw: (obj, ctx, selected) => {
      // triangle 4, at right, top
      ctx.beginPath();
      ctx.moveTo(obj.p1[0], obj.p1[1]); // pick up "pen," reposition at 500 (horiz), 0 (vert)
      ctx.lineTo(obj.p2[0], obj.p2[1]); // draw straight down by 200px (200 + 200)
      ctx.lineTo(obj.p3[0], obj.p3[1]); // draw up toward left (100 less than 300, so left)
      ctx.closePath(); // connect end to start
      ctx.fillStyle = obj.fill;
      ctx.fill(); // outline the shape that's been described
      if (selected) {
        ctx.lineWidth = 1;
        ctx.strokeStyle = obj.stroke;
        ctx.stroke();
        drawScaler(ctx, obj.p1[0], obj.p1[1]);
        drawScaler(ctx, obj.p2[0], obj.p2[1]);
        drawScaler(ctx, obj.p3[0], obj.p3[1]);
      }
    },
  },
  txt: {
    draw: (obj, ctx, selected) => {
      ctx.fillStyle = obj.fill;
      ctx.font = `${obj.size}px Arial`;
      ctx.fillText(obj.text, obj.x, obj.y);
      if (selected) {
        obj.rect.draw(ctx, selected);
        drawScaler(ctx, obj.rect.x, obj.rect.y);
      }
    },
  },
  line: {
    draw: (obj, ctx, selected) => {
      ctx.strokeStyle = obj.fill;
      ctx.beginPath(); // new segment
      ctx.moveTo(obj.x1, obj.y1); // start is current point
      ctx.lineTo(obj.x2, obj.y2); // start is current point
      ctx.closePath();
      ctx.lineWidth = 2;
      ctx.stroke();
      if (selected) {
        ctx.strokeStyle = obj.stroke;
        ctx.lineWidth = 4;
        ctx.stroke();
        drawScaler(ctx, obj.x1, obj.y1);
        drawScaler(ctx, obj.x2, obj.y2);
      }
    },
  },
  // function to draw the polygon on the canvas
  ngon: {
    draw: (obj, ctx, selected) => {
      ctx.beginPath();
      ctx.moveTo(
        obj.x + obj.size * Math.cos(2 * Math.PI * 0 / obj.n),
        obj.y + obj.size * Math.sin(2 * Math.PI * 0 / obj.n)
      );
      obj.edges.forEach((e) => {
        ctx.lineTo(e[0], e[1]);
      });
      ctx.fillStyle = obj.fill;
      ctx.fill();
      if (selected) {
        ctx.lineWidth = 1;
        ctx.strokeStyle = obj.stroke;
        ctx.stroke();
        obj.edges.forEach((e) => {
          drawScaler(ctx, e[0], e[1]);
        });
      }
      ctx.closePath();
    },
  },
  star: {
    draw: (obj, ctx, selected) => {
      let rot = Math.PI / 2 * 3;
      let x = obj.x;
      let y = obj.y;
      let step = Math.PI/obj.spikes;
      ctx.beginPath();
      ctx.moveTo(obj.x,obj.y-obj.outerRadius)
      for (let i = 0; i < obj.spikes; i++) {
        x = obj.x + Math.cos(rot) * obj.outerRadius;
        y = obj.y + Math.sin(rot) * obj.outerRadius;
        ctx.lineTo(x, y)
        rot += step;
        x = obj.x + Math.cos(rot) * obj.innerRadius;
        y = obj.y + Math.sin(rot) * obj.innerRadius;
        ctx.lineTo(x, y)
        rot += step;
      }
      ctx.lineTo(obj.x, obj.y - obj.outerRadius);
      ctx.closePath();
      ctx.fillStyle = obj.fill;
      ctx.fill();
      if (selected) {
        ctx.lineWidth = 1;
        ctx.strokeStyle = obj.stroke;
        ctx.stroke();
        drawScaler(ctx, obj.x, obj.y - obj.outerRadius);
      }
    },
  },
};


export default drawingStrategies;
